import { Box } from '@mui/material';
import React, { useState } from 'react';
import Main from './Components/Main';
import Navbar from './Components/Navbar';
import UserList from './Components/UserList';
import Modal from '@mui/material/Modal'
import Form from './Components/Form';
import EditForm from './Components/EditForm';

function App() {
  const [users, setUsers] = useState([]);
  const [targetUser, setTargetUser] = useState()
  const [keyword, setKeyword] = useState('')
  const [addModal, setAddModal] = useState(false);
  const [editModal, setEditModal] = useState(false);
  // Modal Handles
  const handleEditOpen = () => setEditModal(true);
  const handleEditClose = () => setEditModal(false);
  const handleAddOpen = () => setAddModal(true);
  const handleAddClose = () => setAddModal(false);

  const getHobby = () => {
    return users.map((user) => user.hobby)
  }

  const addUser = (name, address, hobby) => {
    setUsers([
      ...users,
      {id: +new Date(), name, address, hobby}
    ])
  }

  const editUser = (id, name, address, hobby) => {
    const newUsers = users.filter((user) => user.id !== id)
    setUsers([
      ...newUsers,
      {name, address, hobby}
    ])
  }

  const openEdit = (user) => {
    setTargetUser(user);
    handleEditOpen();
  }

  const searchUser = () => {
    return users.filter((user) => user.name.toLowerCase().includes(keyword.toLowerCase()))
  }

  return (
    <Box sx={{ maxWidth: 800, mx: 'auto' }}>
      <Navbar keyword={keyword} onChangeKeyword={(e) => setKeyword(e)} handleOpen={handleAddOpen} />
      {(users.length ? <UserList openEdit={openEdit} users={(!keyword ? users : searchUser())} /> : <Main />)}

      {/* Add Modal */}
      <Modal open={addModal} onClose={handleAddClose}>
        <Form addUser={addUser} handleClose={handleAddClose} />
      </Modal>
      {/* Edit Modal */}
      <Modal open={editModal} onClose={handleEditClose}>
        <EditForm user={targetUser} editUser={editUser} handleClose={handleEditClose} />
      </Modal>
    </Box>
  )
}

export default App;