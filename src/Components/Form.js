import { Box, Button, TextField, Typography } from "@mui/material";
import React, { useState } from "react";

const style = {
  alignText: "center",
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

export default function Form({ addUser, handleClose }) {
  const [name, setName] = useState("");
  const [address, setAddress] = useState("");
  const [hobby, setHobby] = useState("");

  return (
    <Box
      component="form"
      sx={style}
      onSubmit={(e) => {
        e.preventDefault();
        addUser(name, address, hobby);
        handleClose();
      }}
    >
      <Typography mb={2} textAlign="center" variant="h5">
        Add Users
      </Typography>
      <TextField
        required
        fullWidth
        sx={{ mb: 2 }}
        onChange={(e) => setName(e.target.value)}
        id="name"
        variant="outlined"
        label="name"
        value={name}
      />
      <TextField
        required
        fullWidth
        sx={{ mb: 2 }}
        onChange={(e) => setAddress(e.target.value)}
        id="address"
        variant="outlined"
        label="address"
        value={address}
      />
      <TextField
        required
        fullWidth
        sx={{ mb: 2 }}
        onChange={(e) => setHobby(e.target.value)}
        id="hobby"
        variant="outlined"
        label="hobby"
        value={hobby}
      />
      <Button
        fullWidth
        sx={{ display: "block", mx: "auto" }}
        type="submit"
        variant="outlined"
      >
        Save
      </Button>
    </Box>
  );
}
