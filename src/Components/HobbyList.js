import React from "react";
import List from "@mui/material/List";
import { Divider, ListItem, ListItemText, Typography } from "@mui/material";

export default function HobbyList({ hobbies }) {
  return (
    <>
      <Typography variant="h4" component="h1" mt={-4} mb={4}>
        Hobby List
      </Typography>
      <Divider variant="middle" />
      <List sx={{ width: "100%", bgcolor: "background.paper", px: 2 }}>
        {hobbies.map((hobby, i) => (
          <>
            <ListItem key={i} id={i}>
              <ListItemText
                sx={{ flexBasis: 1, fontSize: "1.3em" }}
                primary={hobby}
              />
            </ListItem>
            <Divider variant="fullWidth" />
          </>
        ))}
      </List>
    </>
  );
}
