import { Box, Typography } from "@mui/material";
import React from "react";

export default function Main() {
  return (
    <Box
      component="div"
      sx={{
        height: "80vh",
        width: "100%",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Typography variant="h5" component="h5" sx={{ textAlign: "center" }}>
        There's no users currently
      </Typography>
      <Typography variant="p" component="p" sx={{ textAlign: "center" }}>
        Go add some users...
      </Typography>
    </Box>
  );
}
