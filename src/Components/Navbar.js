import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import React, { useContext } from "react";
import { NavLink } from "react-router-dom";
import { CommonContext } from "../Context/CommonContext";

export default function Navbar({ handleOpen }) {
  const { value } = useContext(CommonContext);


  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar sx={{backgroundColor: value.color}} position="static">
        <Toolbar sx={{ display: "flex" }}>
          <NavLink
            style={({ isActive }) => {
              return {
                color: isActive ? "red" : "whitesmoke",
                textDecoration: "none",
                marginRight: 40,
                flexBasis: "30%",
              };
            }}
            to="/"
          >
            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
              My App
            </Typography>
          </NavLink>
          <NavLink
            style={({ isActive }) => {
              return {
                color: isActive ? "red" : "whitesmoke",
                textDecoration: "none",
                marginRight: 30,
                flexBasis: "20%",
              };
            }}
            to="/about"
          >
            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
              About
            </Typography>
          </NavLink>
          <NavLink
            style={({ isActive }) => {
              return {
                color: isActive ? "red" : "whitesmoke",
                textDecoration: "none",
                marginRight: 8,
                flexBasis: "20%",
              };
            }}
            to="/hobby"
          >
            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
              Hobby
            </Typography>
          </NavLink>

          <Button
            sx={{ flexBasis: "20%" }}
            type="button"
            onClick={() => handleOpen()}
            color="inherit"
          >
            Add User
          </Button>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
