import {
  List,
  ListItem,
  ListItemText,
  Divider,
  Button,
  Typography,
} from "@mui/material";
import React from "react";
import { useNavigate } from "react-router-dom";

export default function UserList({ users, openEdit, openView }) {
  const navigate = useNavigate();

  return (
    <List sx={{ width: "100%", bgcolor: "background.paper", px: 2 }}>
      {users.map((user) => (
        <>
          <ListItem key={user.id} id={user.id}>
            <ListItemText
              sx={{ flexBasis: "80%" }}
              primary={user.name}
              secondary={user.address}
            />
            <ListItemText
              primary={
                <Typography variant="h6" textAlign="center">
                  {user.hobby}
                </Typography>
              }
              secondary={
                <>
                  <Button
                    sx={{ display: "inline-block", my: 2 }}
                    onClick={() => openEdit(user)}
                    fullWidth
                    type="button"
                    variant="outlined"
                  >
                    Edit
                  </Button>
                  <Button
                    sx={{ display: "inline-block" }}
                    onClick={() => navigate(`/view/${user.id}`)}
                    fullWidth
                    type="button"
                    variant="outlined"
                  >
                    View
                  </Button>
                </>
              }
            />
          </ListItem>
          <Divider variant="fullWidth" />
        </>
      ))}
    </List>
  );
}
