import React, { createContext, useReducer } from 'react';

export const AuthContext = createContext();

const initialState = {
  isAuthenticated: false,
  user: null,
  password: null,
};

const reducer = (state, action) => {
  switch(action.type) {
    case "LOGIN":
      localStorage.setItem("user", JSON.stringify(action.user));
      localStorage.setItem("password", JSON.stringify(action.password));
      return {
        ...state,
        isAuthenticated: true,
        user: action.user,
        password: action.password
      }
    case "LOGOUT":
      localStorage.clear();
      return {
        ...state,
        isAuthenticated: false,
        user: null
      }
    default:
      return state;
  }
}

export const AuthProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <AuthContext.Provider value={{state, dispatch}}>
      {children}
    </AuthContext.Provider>
  )
}


