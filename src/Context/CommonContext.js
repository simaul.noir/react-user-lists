import { createContext, useState } from "react";

export const CommonContext = createContext();

const initalValue = {
  title: "My App",
  color: "#0971f1"
}

export default function CommonProvider({ children }) {
  const [value, setValue] = useState(initalValue);

  const getAbout = () => {
    setValue({
      title: "About Page",
      color: "#57C5B6"
    })
  }

  const getHobbyContext = () => {
    setValue({
      title: "Hobby Page",
      color: "#00235B"
    })
  }

  const getHome = () => {
    setValue({
      title: "My App",
      color: "#539165"
    })
  }

  const getView = () => {
    setValue({
      title: "View Page",
      color: "#2E3840"
    })
  }

  return (
    <CommonContext.Provider value={{value, getAbout, getHobbyContext, getHome, getView}}>
      {children}
    </CommonContext.Provider>
  )
}