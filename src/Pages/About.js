import { Box, Container, Divider, Typography } from '@mui/material'
import React, { useEffect, useContext } from 'react'
import { CommonContext } from '../Context/CommonContext';
import { Helmet } from 'react-helmet';

export default function About() {
  const { value, getAbout } = useContext(CommonContext)
  
  useEffect(() => {
    getAbout();
  }, []);

  return (

    <Box width="100%" p={8}>
      <Helmet>
        <title>{value.title}</title>
      </Helmet>
      <Typography variant="h4" component='h1' mt={-4} mb={4}  >About Us</Typography>
      <Divider variant="middle" sx={{mb: 4}} />
      <Typography variant="p">
        Ini adalah aplikasi web untuk menambahkan nama, alamat, dan hobi. Ini mempermudah pengguna untuk menyimpan dan mengelola informasi tentang orang lain. Fitur-fitur yang tersedia antara lain menambah, mencari, mengedit, dan menghapus data. Aplikasi ini meningkatkan produktivitas pengguna dalam melakukan tugas-tugas lainnya
      </Typography>
    </Box>
  )
}
