import { Box } from '@mui/material';
import React, { useEffect, useContext } from 'react'
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import HobbyList from '../Components/HobbyList';
import Main from '../Components/Main';
import { CommonContext } from '../Context/CommonContext';
import { Helmet } from 'react-helmet';


export default function Hobby({ getHobby }) {
  const { value, getHobbyContext } = useContext(CommonContext)
  
  useEffect(() => {
    getHobbyContext();
  }, []);
  
  let hobby = getHobby();  

  return (
    <><Helmet>
      <title>{value.title}</title>
    </Helmet><Box p={8} width="100%">
        {(!hobby.length ? <Main /> : <HobbyList hobbies={hobby} />)}
      </Box></>
  )
}
