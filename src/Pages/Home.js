import { Box, Divider } from '@mui/material';
import Modal from '@mui/material/Modal';
import React, { useContext, useEffect } from 'react';
import EditForm from '../Components/EditForm';
import Form from '../Components/Form';
import Main from '../Components/Main';
import SearchBar from '../Components/SearchBar';
import UserList from '../Components/UserList';
import { CommonContext } from '../Context/CommonContext';
import { Helmet } from 'react-helmet';

function Home({ 
  users,
  openEdit,
  keyword,
  searchUser,
  addModal,
  handleAddClose,
  addUser,
  editModal,
  handleEditClose,
  targetUser,
  editUser,
  onChangeKeyword,
  openView
 }) {
  const { value, getHome } = useContext(CommonContext)
  
  useEffect(() => {
    getHome();
  }, []);

  return (
    <Box sx={{ width: "100%" }}>
      <Helmet>
        <title>{value.title}</title>
      </Helmet>
      <SearchBar keyword={keyword} onChangeKeyword={onChangeKeyword} />
      <Divider variant='fullWidth' />
      {(users.length ? <UserList openEdit={openEdit} users={(!keyword ? users : searchUser())} openView={openView} /> : <Main />)}

      {/* Add Modal */}
      <Modal open={addModal} onClose={handleAddClose}>
        <Form addUser={addUser} handleClose={handleAddClose} />
      </Modal>
      {/* Edit Modal */}
      <Modal open={editModal} onClose={handleEditClose}>
        <EditForm user={targetUser} editUser={editUser} handleClose={handleEditClose} />
      </Modal>
    </Box>
  )
}

export default Home;