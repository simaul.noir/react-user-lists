import { Box, Typography } from '@mui/material'
import React, { useState, useEffect, useContext } from 'react'
import { useParams } from 'react-router-dom'
import { CommonContext } from '../Context/CommonContext'
import { Helmet } from 'react-helmet';

export default function View({ getUsers }) {
  const { value, getView } = useContext(CommonContext)
  
  useEffect(() => {
    getView();
  }, []);

  let { id } = useParams();
  let [user] = useState(getUsers(id));

  console.log(id, user)

  return (
    <Box p={8}>
      <Helmet>
        <title>{value.title}</title>
      </Helmet>
      <Typography variant="h3" component="h1">Nama: {user.name}</Typography>
      <Typography variant="h4" component="h2">Alamat: {user.address}</Typography>
      <Typography variant="h4" component="h2">Hobi: {user.hobby}</Typography>
    </Box>
  )
}
