/* eslint-disable eqeqeq */
import { Container } from "@mui/material";
import React, { useState } from "react";
import { Route, Routes, useNavigate } from "react-router-dom";
import Navbar from "./Components/Navbar";
import About from "./Pages/About";
import Hobby from "./Pages/Hobby";
import Home from "./Pages/Home";
import View from "./Pages/View";

export default function UserApp() {
  const navigate = useNavigate();
  const [users, setUsers] = useState([]);
  const [targetUser, setTargetUser] = useState();
  const [keyword, setKeyword] = useState("");
  const [addModal, setAddModal] = useState(false);
  const [editModal, setEditModal] = useState(false);
  // Modal Handles
  const handleEditOpen = () => setEditModal(true);
  const handleEditClose = () => setEditModal(false);
  const handleAddOpen = () => {
    navigate("/");
    setAddModal(true);
  };
  const handleAddClose = () => setAddModal(false);

  const getHobby = () => {
    return users.map((user) => user.hobby);
  };

  const getUsers = (id) => {
    return users.find((user) => user.id == id);
  };

  const addUser = (name, address, hobby) => {
    setUsers([...users, { id: +new Date(), name, address, hobby }]);
  };

  const editUser = (id, name, address, hobby) => {
    const newUsers = users.filter((user) => user.id !== id);
    setUsers([...newUsers, { name, address, hobby }]);
  };

  const openEdit = (user) => {
    setTargetUser(user);
    handleEditOpen();
  };

  const searchUser = () => {
    return users.filter((user) =>
      user.name.toLowerCase().includes(keyword.toLowerCase())
    );
  };

  const onChangeKeyword = (e) => {
    setKeyword(e);
  };

  return (
    <Container maxWidth="md">
      <Navbar handleOpen={handleAddOpen} />
      <Routes>
        <Route
          path="/"
          element={
            <Home
              users={users}
              openEdit={openEdit}
              keyword={keyword}
              searchUser={searchUser}
              addModal={addModal}
              handleAddClose={handleAddClose}
              addUser={addUser}
              editModal={editModal}
              handleEditClose={handleEditClose}
              targetUser={targetUser}
              editUser={editUser}
              onChangeKeyword={onChangeKeyword}
            />
          }
        />
        <Route path="/about" element={<About />} />
        <Route path="/hobby" element={<Hobby getHobby={getHobby} />} />
        <Route path="/view/:id" element={<View getUsers={getUsers} />} />
      </Routes>
    </Container>
  );
}
